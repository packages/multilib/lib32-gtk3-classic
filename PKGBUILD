# Maintainer: Bernhard Landauer <oberon@manjaro.org>
# Maintainer: Tomasz Gąsior <tomaszgasior.pl>

_repo=gtk3-mushrooms
_commit=a02de1c9d9ed99e51ae4470c28644d3222a0bf8f # tags/3.24.4^0
__arch_pkg_commit=c60087d9efa09603917116d34474adb79f400b96
_pkgname=lib32-gtk3
pkgname=$_pkgname-classic
pkgver=3.24.5
pkgrel=1
pkgdesc="\"gtk3-mushrooms\"; GTK3 patched for classic desktops. See README."
url="https://github.com/TomaszGasior/gtk3-mushrooms"
_archgit=https://git.archlinux.org/svntogit/community.git/plain/trunk
conflicts=($_pkgname)
provides=($_pkgname=$pkgver)
arch=(x86_64)
license=(LGPL)
depends=(
    adwaita-icon-theme
    cantarell-fonts
    "gtk3-classic>=${pkgver}"
    lib32-at-spi2-atk
    lib32-colord
    lib32-dbus
    lib32-dconf
    lib32-gdk-pixbuf2
    lib32-json-glib
    lib32-libcanberra
    lib32-libcups
    lib32-libepoxy
    lib32-librsvg
    lib32-libxcomposite
    lib32-libxcursor
    lib32-libxdamage
    lib32-libxinerama
    lib32-libxkbcommon
    lib32-libxrandr
    lib32-mesa
    lib32-pango
    lib32-rest
    lib32-wayland
    )
makedepends=(
    'git'
    'gcc-multilib'
    'gobject-introspection'
    'gtk-doc'
    'sassc'
    )
source=(
    # GTK source code.
    #"git+https://gitlab.gnome.org/GNOME/gtk.git#commit=$_commit"
    "https://download.gnome.org/sources/gtk+/${pkgver%.*}/gtk+-$pkgver.tar.xz"
    "gtk-query-immodules-3.0-32.hook::$_archgit/gtk-query-immodules-3.0-32.hook?h=packages/$_pkgname&id=$__arch_pkg_commit"
    # gtk3-mushrooms patches
    "git+$url.git"
)
sha256sums=('0be5fb0d302bc3de26ab58c32990d895831e2b7c7418d0ffea1206d6a3ddb02f'
            '4ac8112ac7e6fa879756e1eeb89b5efa0825ba00e5b05469913b256f86a37608'
            'SKIP')

__patch_makefiles() {
    __replace_string_in_file() {
        sed -i".bak" "s/$1/$2/" "$3"
        rm "$3.bak"
    }

    __replace_string_in_file \
        "SRC_SUBDIRS = gdk gtk libgail-util modules demos tests testsuite examples" \
        "SRC_SUBDIRS = gdk gtk libgail-util modules demos" \
        "Makefile.am"

    __replace_string_in_file \
        "SUBDIRS = po po-properties \$(SRC_SUBDIRS) docs m4macros build" \
        "SUBDIRS = po \$(SRC_SUBDIRS) m4macros build" \
        "Makefile.am"

    __replace_string_in_file \
        "SUBDIRS = gtk-demo widget-factory icon-browser" \
        "SUBDIRS = widget-factory" \
        "demos/Makefile.am"

	__replace_string_in_file \
		"gtk-update-icon-cache" \
		"" \
		"gtk/Makefile.am"
}

__patch_gtk_code() {
    for patch_file in $srcdir/$_repo/*.patch; do
        patch -p 2 -i "$patch_file"
    done

    cat "$srcdir/$_repo/smaller-adwaita.css" | tee -a gtk/theme/Adwaita/gtk-contained{,-dark}.css > /dev/null
}

#pkgver() {
#    cd "$srcdir/gtk"
#    git describe --tags | sed 's/-/+/g'
#}

prepare() {
    cd "$srcdir/gtk+-$pkgver"

    # Make building faster by skipping tests, code examples and unused elements.
    __patch_makefiles

    # Apply patches to GTK code.
    __patch_gtk_code

    NOCONFIGURE=1 ./autogen.sh
}

build() {
    cd "$srcdir/gtk+-$pkgver"

    export CC='gcc -m32'
    export CXX='/bin/false'
    export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

    ./configure \
      --prefix=/usr \
      --libdir=/usr/lib32 \
      --sysconfdir=/etc \
      --localstatedir=/var \
      --enable-x11-backend \
      --enable-wayland-backend \
      --disable-schemas-compile \
      --disable-gtk-doc-html \
      --disable-libcanberra

    # https://bugzilla.gnome.org/show_bug.cgi?id=655517
    sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
    make
}

package() {
    cd "$srcdir/gtk+-$pkgver"

    make DESTDIR="$pkgdir" install

    mv "${pkgdir}"/usr/bin/gtk-query-immodules-3.0{,-32}
    mv "${pkgdir}"/usr/bin/gtk-query-settings{,-32}
    rm "${pkgdir}"/usr/bin/{gtk-{builder-tool,encode-symbolic-svg,launch},gtk3-widget-factory}
    rm -rf "${pkgdir}"/{etc,usr/{include,share}}

    install -Dm 644 ../gtk-query-immodules-3.0-32.hook "${pkgdir}"/usr/share/libalpm/hooks/gtk-query-immodules-3.0-32.hook
}
